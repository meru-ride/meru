import CategoryIcon from '@mui/icons-material/Category'
import { SvgIconProps } from '@mui/material/SvgIcon'

type TrackComponentIconProps = SvgIconProps & {}

export const TrackComponentIcon = (props: TrackComponentIconProps) => <CategoryIcon {...props} />

export default TrackComponentIcon
