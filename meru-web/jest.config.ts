import { compilerOptions } from './tsconfig.json'
import { pathsToModuleNameMapper } from 'ts-jest'

export default {
  preset: 'ts-jest/presets/js-with-ts',
  testEnvironment: 'jsdom',
  moduleNameMapper: {
    ...pathsToModuleNameMapper(compilerOptions.paths, { prefix: '<rootDir>/' }),
    '^.+\\.(css|less)$': '<rootDir>/tests/stylesMock.js',
  },
  setupFilesAfterEnv: ['<rootDir>/tests/jest.setup.ts'],
  watchPathIgnorePatterns: ['pacts/', 'logs/'],
}
