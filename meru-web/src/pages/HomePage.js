import React, { useEffect } from 'react'

import Page from '/src/components/Layout/Page'

import RegisterEquipmentModal from '/src/components/Equipment/RegisterEquipmentModal'
import EquipmentList from '../containers/Equipment/EquipmentListContainer'
import Services, { runMigrations } from '../services'
import Button from '@mui/material/Button'

const HomePage = () => {
  const handleMigrations = () => {
    runMigrations(Services)
  }

  return (
    <Page>
      <Button onClick={handleMigrations}>Migrate!</Button>
      <RegisterEquipmentModal data-test-id="register-equipment-modal" />

      <EquipmentList data-test-id="equipments" />
    </Page>
  )
}

export default HomePage
