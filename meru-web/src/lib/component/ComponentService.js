import * as uuid from 'uuid'

import EventType from '/src/lib/history/EventType'

class ComponentService {
  constructor(store) {
    this.store = store
  }

  all() {
    return this.store.getEquipments()
  }

  findById(componentId) {
    return this.store.getComponent(componentId)
  }

  register(newComponent) {
    const component = {
      id: newComponent.id, // uuid.v4(),
      owner_id: newComponent.owner_id,
      name: newComponent.name,
      thumbnail: newComponent.thumbnail,
      usage: {
        time: newComponent.time,
        distance: newComponent.distance,
      },
    }

    return this.store
      .setComponent({ ...component, history: { events: [] } })
      .then(() => this._recordEvent(component.id, EventType.COMPONENT_REGISTERED, { ...component }))
  }

  _recordEvent(componentId, eventType, payload) {
    return this.store.getComponent(componentId).then((component) => {
      return this.store.setComponent({
        ...component,
        history: {
          ...component.history,
          events: [
            ...component.history.events,
            { id: uuid.v4(), date: new Date(), event_type: eventType, payload },
          ],
        },
      })
    })
  }
}

export default ComponentService
