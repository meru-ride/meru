import { Pact } from '@pact-foundation/pact'

import MeruApi from '/src/services/meru_api'

describe('DummyService', () => {
  const provider = new Pact({
    consumer: 'MeruWeb',
    provider: 'DummyService',
  })

  let dummyService

  beforeAll(async () => {
    await provider.setup()

    const api = MeruApi.configure({ port: provider.opts.port })
    dummyService = api.dummies
  })

  afterEach(() => {
    provider.verify()
  })

  afterAll(() => {
    provider.finalize()
  })

  describe('when getting dummies', () => {
    const DUMMY_BODY = [{ name: 'dummy1' }, { name: 'dummy2' }]

    beforeEach(() => {
      provider.addInteraction({
        state: 'I have a list of dummies',
        uponReceiving: 'get dummies',
        withRequest: {
          method: 'GET',
          path: '/dummies',
          headers: { Accept: 'application/json' },
        },
        willRespondWith: {
          status: 200,
          headers: { 'Content-Type': 'application/json' },
          body: DUMMY_BODY,
        },
      })
    })

    it('returns all dummies', async () => {
      const dummies = await dummyService.all()

      expect(dummies).toEqual(DUMMY_BODY)
    })
  })
})
