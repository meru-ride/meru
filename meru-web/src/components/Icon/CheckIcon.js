import React from 'react'

import FactCheckIcon from '@mui/icons-material/FactCheck'

const CheckIcon = (props) => <FactCheckIcon {...props} />

export default CheckIcon
