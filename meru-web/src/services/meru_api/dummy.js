class DummyService {
  constructor(api) {
    this.api = api
  }

  all() {
    return this.api.get('dummies')
  }
}
export default DummyService
