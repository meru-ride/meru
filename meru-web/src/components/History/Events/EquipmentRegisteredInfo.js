import React from 'react'

import Typography from '@mui/material/Typography'

const EquipmentRegisteredInfo = ({ equipment }) => (
  <Typography>{equipment.name} registered</Typography>
)

export default EquipmentRegisteredInfo
