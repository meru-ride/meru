import React from 'react'

import FactCheckIcon from '@mui/icons-material/FactCheck'

const RegisterComponentIcon = (props) => <FactCheckIcon {...props} />

export default RegisterComponentIcon
