import firebase from 'firebase/compat/app'
import 'firebase/compat/auth'

firebase.initializeApp({
  apiKey: 'AIzaSyBJ0eARQM1jSb1H2T0ipDlJr32WZxhOw5k',
  authDomain: 'meru-ride.firebaseapp.com',
  projectId: 'meru-ride',
  storageBucket: 'meru-ride.appspot.com',
  messagingSenderId: '387280053815',
  appId: '1:387280053815:web:ef1269febeaa80e3f6c1b2',
})

export const auth = firebase.auth()

auth.useEmulator('http://localhost:9099')
