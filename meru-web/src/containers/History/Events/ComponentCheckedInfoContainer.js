import React from 'react'

import Async from '/src/components/Layout/Async'
import Loading from '/src/components/Layout/Loading'
import ComponentCheckedInfo from '/src/components/History/Events/ComponentCheckedInfo'

import Services from '/src/services'

const ComponentCheckedInfoContainer = ({ component }) => {
  const SmallLoading = <Loading size="medium" inline />

  const request = () =>
    Services.components.findById(component.id).then((component) => ({ component }))

  return (
    <Async
      loading={SmallLoading}
      request={request}
      success={ComponentCheckedInfo}
      data-test-id="info"
    />
  )
}

export default ComponentCheckedInfoContainer
