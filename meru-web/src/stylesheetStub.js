class StylesheetStub {
  constructor() {
    return new Proxy(this, {
      get: function (stub, property) {
        return property
      },
    })
  }
}

const stub = new StylesheetStub()

export default stub
