import React from 'react'

import RegisterEquipmentForm from '/src/components/Equipment/Form/RegisterEquipmentForm'

import { useAuthentication } from '/src/contexts/authentication'

import Services from '/src/services'

const RegisterEquipmentFormContainer = ({ id, onSubmit }) => {
  const { user } = useAuthentication()

  const handleSubmit = (equipment) => {
    const newEquipment = { ...equipment, owner_id: user.uid }

    Services.equipments.register(newEquipment).then(() => onSubmit && onSubmit(newEquipment))
  }

  return <RegisterEquipmentForm id={id} onSubmit={handleSubmit} data-test-id="form" />
}

export default RegisterEquipmentFormContainer
